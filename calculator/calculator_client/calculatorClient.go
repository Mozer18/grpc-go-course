package main

import (
	"log"
	context "golang.org/x/net/context"
	"fmt"
	calculatorpb "grpc-go-course/calculator/calculatorpb"
	grpc "google.golang.org/grpc"
)



func main() {
	conn, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("could not connect: %v", err)
	}
	// close the connction when everything is finished
	defer conn.Close()

	c :=calculatorpb.NewCalculatorServiceClient(conn)
	doCalculation(c)
}

func doCalculation(c calculatorpb.CalculatorServiceClient){
	fmt.Println("Starting calculation using unary RPC...")
	request := &calculatorpb.CalculatorRequest{
		Arguments : &calculatorpb.Arguments{
			Arg1 : 23,
			Arg2 : 15,
		},
	}

	result,  err := c.Calculator(context.Background(), request)
	if err != nil{
		log.Fatalf("Error calling the Calculator RPC: %v", err)
	}

	log.Println("Response form Calculator RPC: %v", result.Result)
}