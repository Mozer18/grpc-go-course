package main

import (
	calculatorpb "grpc-go-course/calculator/calculatorpb"
	context "golang.org/x/net/context"
	"log"
	grpc "google.golang.org/grpc"
	"net"
	"fmt"
)


type calculatorServer struct{}

func (*calculatorServer) Calculator(ctx context.Context, req *calculatorpb.CalculatorRequest) (*calculatorpb.CalculatorResponse, error){
	fmt.Println("Starting to do calcultion with %v",req.GetArguments())

	firstArg  := req.GetArguments().GetArg1()
	secondArg := req.GetArguments().GetArg2()

	result := firstArg + secondArg
	res := &calculatorpb.CalculatorResponse{
		Result : result,
	}

	return res , nil
}

func main() {
	fmt.Println("Starting claculator server....")
	
	lis, err := net.Listen("tcp", "0.0.0.0:50051")
	if err != nil {
		log.Fatalf("Failed to listen %v", err)
	}

	s := grpc.NewServer() 
    calculatorpb.RegisterCalculatorServiceServer(s, &calculatorServer{})
	if err := s.Serve(lis); err != nil{
		log.Fatalf("Failed to listen %v", err)
	}
	
}