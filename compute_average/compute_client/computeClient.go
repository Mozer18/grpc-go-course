package main

import (
	"time"
	"golang.org/x/net/context"
	"grpc-go-course/compute_average/computepb"
	"log"
	"google.golang.org/grpc"
)


func main()  {
	//Establish the client connection
	grpcClient, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Error while establishing connection with server %v", err)
	}

	defer grpcClient.Close()
    //Create a new compute service client
	computeServiceClient := computepb.NewComputeServiceClient(grpcClient)

	doCompute(computeServiceClient)
}

func doCompute(c computepb.ComputeServiceClient)  {
	log.Printf("Starting to do client streaming...")

/*	requests := []*computepb.ComputeRequest{
		&computepb.ComputeRequest{
			Number : 1,
		},
		&computepb.ComputeRequest{
			Number : 2,
		},		
		&computepb.ComputeRequest{
			Number : 3,
		},
		&computepb.ComputeRequest{
			Number : 4,
		},		
	}
*/
	numbers :=[]float32{12,22,32,42}
	//Calling the compute rpc service
	resStream, err :=  c.Compute(context.Background())
	if err != nil{
		log.Fatalf("Error while calling the compute rpc service: %v", err)
	}
	//Send all requests
	for _, number := range numbers {
		log.Printf("Request value %v", number)
		resStream.Send(&computepb.ComputeRequest{
			Number : number,
		})
		time.Sleep(1000 * time.Millisecond)
		
	}

	response, err := resStream.CloseAndRecv() 
	if err != nil{
		log.Fatalf("Error while receiving the response from compute rpc service %v", err)
	}

	log.Printf("Average: %v", response.GetResult())
	
}