package main

import (
	"log"
	"io"
	"google.golang.org/grpc"
	"grpc-go-course/compute_average/computepb"
	"net"
	"fmt"
)

type compute_server struct{}

func (*compute_server) Compute(stream computepb.ComputeService_ComputeServer) error{
	fmt.Printf("Compute function was invoked with a streaming request.")
	result := float32(0)
	k :=float32(0)
	for{
		number, err := stream.Recv()
		if err == io.EOF{
			//End of client streaming 
			result /= k
			return stream.SendAndClose(&computepb.ComputeResponse{
				Result : result,
			})
			
		}
		if err != nil {
			log.Fatalf("Error while receiving data %v", err)
		}
		result += number.GetNumber()
		k++
	}
}
func main()  {
	fmt.Printf("Starting compute_average server... \n")
	//Create the listening address 
	lis, err := net.Listen("tcp", "0.0.0.0:50051")
	if err != nil{
		log.Fatalf("Error while create the listening IP address %v", err)
	}

	//Create a grpc server
	s := grpc.NewServer()
	computepb.RegisterComputeServiceServer(s , &compute_server{})	
	
	//run it
	if err := s.Serve(lis); err != nil{
		log.Fatalf("Failed tolisten %v", err)
	}
}