package main

import (
	"time"
	"io"
	"golang.org/x/net/context"
	"fmt"
	"grpc-go-course/find_maximum/findpb"
	"log"
	"google.golang.org/grpc"
)


func main()  {
    //Create socket connection
	conn, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Error while connecting with the server %v \n", err)
	}
	//Close the connection when everything is finished
	defer conn.Close()

	findServiceClient := findpb.NewFindServiceClient(conn)

	doFindMax(findServiceClient)	
}

func doFindMax(serviceClient findpb.FindServiceClient)  {
	fmt.Printf("Starting client streaming rpc!!! \n")

	requests := []float32{1,5,3,6,2,20}

	//Call FindMax rpc service
	findMaxClient, err := serviceClient.FindMax(context.Background())
	if err != nil {
		log.Fatalf("Error while calling FindMax rpc service %v \n", err)
	}

	//Create a channel
	waitC := make(chan struct{})

	//Create a thread to send a bunch of requests from the client
	go func() {
		for _, req := range requests {
			fmt.Printf("Send the number: %v \n", req)
			findMaxClient.Send(&findpb.FindMaxRequest{
				Number : req,
			})	
			time.Sleep(1000 * time.Millisecond)
		}
		findMaxClient.CloseSend()
	}()

	//Create a thread to receive a bunch of responses
	go func() {
		i := 1
		for{
			response,err := findMaxClient.Recv()
			if err == io.EOF{
				//we're reached the end of the stream
				break;
			}
			if err != nil {
				log.Fatalf("Error while receiving data %v \n", err)
				break;
			}
			fmt.Printf("The max number with %d iems in the list is %v\n", i, response.GetMax())
			i++
		}
		close(waitC)
	}()

	//Block until everything is done
	<-waitC


}