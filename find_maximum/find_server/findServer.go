package main

import (
	"io"
	"fmt"
	"grpc-go-course/find_maximum/findpb"
	"google.golang.org/grpc"
	"log"
	"net"
)

type findMaxServer struct {}

func (*findMaxServer) FindMax(stream findpb.FindService_FindMaxServer) error  {
	fmt.Printf("FindMax service was invoked!!! \n")
	//check if it's the first number
    isFirstNumber  := true
    previousNumber :=float32(0) 
	currentNumber  :=float32(0)

	for{
		reqNumber, err := stream.Recv()
		if err == io.EOF{
			return nil
		}
		if err != nil{
			log.Fatalf("Error while receiving %v \n", err)
		}

		
		if isFirstNumber ==true {
			isFirstNumber = false
			previousNumber = reqNumber.GetNumber()
			err := stream.Send(&findpb.FindMaxResponse{
				Max : previousNumber,
			})
			if err != nil {
				return err
			}
			
		}
		if isFirstNumber != true{
			currentNumber = reqNumber.GetNumber()
			if currentNumber > previousNumber {
				previousNumber = currentNumber
				sendErr := stream.Send(&findpb.FindMaxResponse{
					Max : currentNumber,
				})
				if sendErr != nil {
					return sendErr
				}
			}
		}
		
	}
	
}

func main()  {
	fmt.Printf("Starting the server... \n")
	//Create the listening IP address
	lis, err := net.Listen("tcp", "0.0.0.0:50051")
	if err != nil {
		log.Fatalf("Error while create the listening IP address %v \n", err)
	}

	//create a grpc server
	s := grpc.NewServer()

	//Register findMaxServer
	findpb.RegisterFindServiceServer(s, &findMaxServer{})

	//Run the server
	runErr := s.Serve(lis)
	if runErr !=nil {
		log.Fatalf("Error while starting the server %v \n", runErr)
	}
}