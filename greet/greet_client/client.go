package main

import (
	"time"
	"grpc-go-course/greet/greetpb"
	"io"
	"fmt"
	"golang.org/x/net/context"
	"log"
	"google.golang.org/grpc"
)


func main()  {
	
	conn, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("could not connect: %v \n", err)
	}
	// close the connction when everything is finished
	defer conn.Close()

	c := greetpb.NewGreetServiceClient(conn)
	
	// Unary communication
	//doUnary(c)

    // Server streaming
	//doServerStreaming(c)

	//Client streaming
	doClientStreaming(c)
}

func doUnary(c greetpb.GreetServiceClient){
	fmt.Println("Starting to do unary RPC...")
	req := &greetpb.GreetRequest{
		Greeting : &greetpb.Greeting{
			FirstName : "Jean Mathieu",
			LastName  : "Tchamdjeu",
		},
	}
    // Call Greet rpc service
	result, err := c.Greet(context.Background(),req)
	if err != nil{
		log.Fatalf("Error calling the Greet RPC: %v \n", err)
	}

	log.Printf("Response form Greet RPC: %v \n", result.Result)
}

func doServerStreaming(c greetpb.GreetServiceClient){
	fmt.Println("Starting to do server Streaming RPC... \n")	
	req := &greetpb.GreetManyTimesRequest{
		Greeting : &greetpb.Greeting{
			FirstName : "Jean Mathieu",
			LastName  : "Tchamdjeu",
		},
	}
	//Call GreetManyTimes rpc service
	resStream, err := c.GreetManyTimes(context.Background(), req)
	if err != nil {
		log.Fatalf("Error while calling GreetManyTimes RPC : %v \n", err)
	}

	for  {
		msg, err := resStream.Recv()
		if err == io.EOF{
			//We're reached the end of the stream
			break;
		}	
		if err != nil {
			log.Fatalf("Error while reading the stram: %v \n", err)
		}

		log.Println(msg.Result)
	}
	
}

func doClientStreaming(c greetpb.GreetServiceClient)  {
	fmt.Println("Starting to do client Streaming RPC... \n")

	requests := []*greetpb.LongGreetingRequest{
		&greetpb.LongGreetingRequest{
			Greeting : &greetpb.Greeting{
				FirstName : "Jean Mathieu",
				LastName  : "Tchamdjeu",				
			},
		},
		&greetpb.LongGreetingRequest{
			Greeting : &greetpb.Greeting{
				FirstName : "Fabrice",
				LastName  : "Siebafeu",				
			},
		},
		&greetpb.LongGreetingRequest{
			Greeting : &greetpb.Greeting{
				FirstName : "Christian",
				LastName  : "Mbomezomo",				
			},
		},
		&greetpb.LongGreetingRequest{
			Greeting : &greetpb.Greeting{
				FirstName : "Mandela",
				LastName  : "Dzangue",				
			},
		},		
		&greetpb.LongGreetingRequest{
			Greeting : &greetpb.Greeting{
				FirstName : "Alex",
				LastName  : "Sipa",				
			},
		},						
	}
    
	//Call LongGreeting rpc service
	longGreetingClient,err := c.LongGreeting(context.Background())
	if err != nil {
		log.Fatalf("Error while calling the LongGreeting service %v \n", err)
	}

	for _, req :=range requests{
		fmt.Printf("Sending request %v \n", req.GetGreeting())
		longGreetingClient.Send(req)
		time.Sleep(1000 * time.Millisecond)
	}

	response, err := longGreetingClient.CloseAndRecv()
	if err != nil{
		log.Fatalf("Error while receiving the LongGreeting service response %v \n", err)
	}

	log.Printf("response: %v \n", response.GetResult())
	
}

func doBidiStreaming(c greetpb.GreetServiceClient)  {
	fmt.Println("Starting to do client Streaming RPC... \n")
	
	requests := []*greetpb.GreetEveryoneRequest{
		&greetpb.GreetEveryoneRequest{
			Greeting : &greetpb.Greeting{
				FirstName : "Jean Mathieu",
				LastName  : "Tchamdjeu",				
			},
		},
		&greetpb.GreetEveryoneRequest{
			Greeting : &greetpb.Greeting{
				FirstName : "Fabrice",
				LastName  : "Siebafeu",				
			},
		},
		&greetpb.GreetEveryoneRequest{
			Greeting : &greetpb.Greeting{
				FirstName : "Christian",
				LastName  : "Mbomezomo",				
			},
		},
		&greetpb.GreetEveryoneRequest{
			Greeting : &greetpb.Greeting{
				FirstName : "Mandela",
				LastName  : "Dzangue",				
			},
		},		
		&greetpb.GreetEveryoneRequest{
			Greeting : &greetpb.Greeting{
				FirstName : "Alex",
				LastName  : "Sipa",				
			},
		},						
	}
	
	greetEveryoneClient, err := c.GreetEveryone(context.Background())
	if err != nil {
		log.Fatalf("Error while calling the GreetEveryone service %v \n", err)
	}

	

	//Create a wait channel
	waitc := make(chan struct{})
	//We send a bunch of message from the client (go routine)
	go func() {
		//function to send a bunch of messages
		for _, req :=range requests{
			fmt.Printf("Sending request %v \n", req.GetGreeting())
			greetEveryoneClient.Send(req)
			time.Sleep(3000 * time.Millisecond)
		}
		greetEveryoneClient.CloseSend()
	
	}()

	//We receive a bunch of response from the server
	go func() {
		for{
			res, err := greetEveryoneClient.Recv()
			if err == io.EOF{
				break
			}
			if err != nil {
				log.Fatalf("Error while receiving the response %v \n", err)
				break
			}
			fmt.Printf("Response: %v \n", res.GetResult())
			time.Sleep(3000 * time.Millisecond)
		}
		close(waitc)
	}()

	//Block until everything is done
	<-waitc
}