package main

import (	
	"io"
	"time"
	"strconv"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"grpc-go-course/greet/greetpb"
	
	"log"
	"net"
	"fmt"
	
)

type server struct {}

//Implement the server method
func (*server) Greet(ctx context.Context, req *greetpb.GreetRequest) (*greetpb.GreetResponse, error)  {
	fmt.Printf("Greet function was invoked with %v",req)
	firstName := req.GetGreeting().GetFirstName()
	lastName  := req.GetGreeting().GetLastName()
	result := "Hello " + firstName + " " + lastName
	res := &greetpb.GreetResponse{
		Result : result,
	}

	return res, nil
}

//Implement the server method
func (*server) GreetManyTimes(req *greetpb.GreetManyTimesRequest,stream greetpb.GreetService_GreetManyTimesServer) error{

	fmt.Printf("GreetManyTimes function was invoked with %v",req.GetGreeting())
	firstName := req.GetGreeting().GetFirstName()
	lastName  := req.GetGreeting().GetLastName()
	
	for i := 0; i < 10; i++ {
		result := "Hello " + firstName + " " + lastName + " number " + strconv.Itoa(i)	
		res := &greetpb.GreetManyTimesResponse{
			Result : result,
		}
		stream.Send(res)
		time.Sleep(1000 * time.Millisecond)
		
	}
	return nil
}

//Implement the server method
func (*server) LongGreeting(stream greetpb.GreetService_LongGreetingServer) error{
	fmt.Printf("LongGreeting function was invoked with a streaming request.")
	result := ""
	for{
		req, err := stream.Recv()
		if err == io.EOF{
			//we've finished reading the client stream
			return stream.SendAndClose(&greetpb.LongGreetingResponse{
				Result : result,
			})
		}
		if err != nil {
			log.Fatalf("Error while reading the client stream %v", err)
			return err
		}
		result += "Hello " +req.GetGreeting().GetFirstName() +" "+req.GetGreeting().GetLastName()+"! \n"
	}
	
}
//Implement the server method
func (*server) GreetEveryone(stream greetpb.GreetService_GreetEveryoneServer) error  {
	fmt.Printf("GreetEveryone function was invoked with a streaming request.")	
	result := ""

	for{
		req, err := stream.Recv()
		if err == io.EOF {
			return nil
		}
		if err != nil {
			log.Fatalf("Error while reading the client stream %v", err)
		}

		result += "Hello " +req.GetGreeting().GetFirstName() +" "+req.GetGreeting().GetLastName()+"! \n"

		sendErr := stream.Send(&greetpb.GreetEveryoneResponse{
			Result : result,
		})
		if sendErr != nil {
			log.Fatalf("Error while sending the result to the client %v", sendErr)
			return sendErr
		}
	}
}
func main() {
	log.Println("Starting go server...")
	//Create the listening IP address
	lis, err := net.Listen("tcp", "0.0.0.0:50051")
	if err != nil {
		log.Fatalf("Failed tolisten %v", err)
	}
    //Create and register the grpc server
	s := grpc.NewServer() 
	greetpb.RegisterGreetServiceServer(s, &server{})
	

	//run it
	if err := s.Serve(lis); err != nil{
		log.Fatalf("Failed tolisten %v", err)
	}
	
}