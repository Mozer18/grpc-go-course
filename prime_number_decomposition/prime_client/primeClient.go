package main

import (
	"io"
	"golang.org/x/net/context"
	"grpc-go-course/prime_number_decomposition/primepb"
	"google.golang.org/grpc"
	"log"
)



func main()  {
	log.Printf("Starting the client side....")
	//Set cleint connection
	clientConnection, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
	if err != nil{
		log.Fatalf("Failed to connect client to the prime server %v", err)
	}

	//close the connction when everything is finished
	defer clientConnection.Close()

	primeServiceClient := primepb.NewPrimeServiceClient(clientConnection)

	doPrimeStreaming(primeServiceClient)
}

func doPrimeStreaming(primeServiceClient primepb.PrimeServiceClient)  {
	log.Printf("Starting to do server streaming....")
	req := &primepb.PrimeRequest{
		Number : int32(616),
	}
	
	// Create prime client
	primeclient, err := primeServiceClient.Prime(context.Background(), req)
	if err != nil{
		log.Printf("Error while calling Prime RPC: %v", err)
	}

	for{
		primeResponse, err := primeclient.Recv()
		if err == io.EOF{
			break;
		}
		if err != nil {
			log.Fatalf("Error while reading the stream %v", err)
		}
		log.Println(primeResponse.PrimeNumber)
	}
}