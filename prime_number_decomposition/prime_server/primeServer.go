package main

import (
//	"time"
	primepb "grpc-go-course/prime_number_decomposition/primepb"
	"google.golang.org/grpc"
	"net"
	"log"
)

type prime_server struct{}

func (*prime_server) Prime(req *primepb.PrimeRequest, stream primepb.PrimeService_PrimeServer) error{
	log.Printf("Starting prime number decomposition service with the number %v",req.Number)
	k := int32(2)
	number := req.GetNumber()
	for{
		if number <= 1 {
			break;
		}
		if number % k == 0{
			number /=k
			res := &primepb.PrimeResponse{
				PrimeNumber : k,
			}
			stream.Send(res)
			//time.Sleep(1000 * time.Microsecond)
		}else{
			k++
		}
		
	}
	return nil
}
func main()  {
	log.Printf("Starting prime server....")

	//Create the listening IP address
	lis, err := net.Listen("tcp", "127.0.0.1:50051")
	if err!= nil {
		log.Fatalf("Failed to listen %v", err)
	}

	//Create and register to grpc server
	newServer := grpc.NewServer()
	primepb.RegisterPrimeServiceServer(newServer, &prime_server{})

	//run it
	if err := newServer.Serve(lis); err != nil{
		log.Fatalf("Failed tolisten %v", err)
	}

}